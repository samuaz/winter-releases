//
// Created by AZCONA VARGAS, SAMUEL EDUARDO [AG-Contractor/5000] on 2019-10-01.
//

#ifndef WINTER_FIELD_DESCRIPTOR_H
#define WINTER_FIELD_DESCRIPTOR_H

#include <string>
#include <vector>
#include <jdbc/cppconn/sqlstring.h>

namespace winter::descriptor {

        enum FieldType : int {
            INT = 0,
            DOUBLE = 1,
            LONG = 2,
            BOOLEAN = 3,
            STRING = 4,
            DATE = 5,
            ENUM = 6,
            OBJECT = 7
        };

        template<typename T>
        struct TypeName {
            static const char *Get();
        };

        template<>
        struct TypeName<int> {
            static const char *Get();
        };


        template<>
        struct TypeName<std::string> {
            static const char *Get();
        };


        struct AbstractField {
            virtual FieldType getType() = 0;

            virtual std::string getName() = 0;

            virtual std::string getRead() = 0;

            virtual std::string getWrite() = 0;

            virtual ~AbstractField() = default;
        };

        template<typename T>
        class Field : public AbstractField {
        private:
            T _value;
            FieldType _type;
            std::string _name;
            std::string _read;
            std::string _write;

        public:
            Field(T value, FieldType type) : _value(value), _type(type) {}

            T getValue() const;

            FieldType getType() override;

            std::string getName() override;

            std::string getRead() override;

            std::string getWrite() override;

            void setValue(T value);

            ~Field() override = default;
        };


    template<typename T>
    T Field<T>::getValue() const {
        return _value;
    }

    template<typename T>
    FieldType Field<T>::getType() {
        return _type;
    }

    template<typename T>
    std::string Field<T>::getName() {
        return _name;
    }

    template<typename T>
    std::string Field<T>::getRead() {
        return _read;
    }

    template<typename T>
    std::string Field<T>::getWrite() {
        return _write;
    }

    template<typename T>
    void Field<T>::setValue(T value) {
        _value = value;
    }

    template class winter::descriptor::Field<int>;
    template class winter::descriptor::Field<bool>;
    template class winter::descriptor::Field<double>;
    template class winter::descriptor::Field<long>;
    template class winter::descriptor::Field<std::string>;
    template class winter::descriptor::Field<sql::SQLString>;

}
#endif //WINTER_FIELD_DESCRIPTOR_H
