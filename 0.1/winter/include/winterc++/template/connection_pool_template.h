//
// Created by samuel on 06/08/19.
//

#ifndef WINTER_CONNECTION_POOL_H
#define WINTER_CONNECTION_POOL_H

#include <vector>
#include <thread>
#include <memory>
#include <mutex>
#include <algorithm>
#include "../exception/generic/winter_exception.h"
#include "../config/config.h"
#include "../exception/config/config_exception.h"
namespace winter {

    template<typename T>
    class ConnectionPool {

    protected:
        /**
        * initial pool size or minimal pool to init
        */
        unsigned int initialPoolSize{};

        /**
         * max pool size if we need more connections we can create new but only if created connections are less than max pool size
         */
        unsigned int maxPoolSize{};


        /**
         * available connections
         */
        std::vector<T *> connections;

        /**
         * connections in use
         *
         * @return  std::vector<db_impl::Connection >
         *
         */
        std::vector<T *> usedConnections;

    public:

        virtual ~ConnectionPool();

        void initPool(const unsigned int &initialPool, const unsigned int &maxPool);

        virtual T *createConn() = 0;

        /**
        * get first connection available
        * @return db_impl::Connection
        */
        virtual T &getConnection();


        /**
         * this check if connections are available and if we can create new connection
         */
        void checkConnections();


        /**
         * add new connection to the pool
         */
        void addConnections(T *conn);

        /**
         * free connection after their use
         */
        void releaseConnection(T *conn);

        /**
         * move connections from used to available and viceversa
         */
        static void moveConnection(std::vector<T *> &from, std::vector<T *> &to,
                                   T *conn);

        void close();
    };

    template<typename T>
    void ConnectionPool<T>::initPool(const unsigned int &initialPool, const unsigned int &maxPool) {

        /// get the initial and max poolsize
        initialPoolSize = initialPool;
        maxPoolSize = maxPool;
        auto db_pool = [&]() {
            try {
                /// get actual total connections
                size_t actualConnections = connections.size() + usedConnections.size();
                /// create the database connections
                if (actualConnections < maxPoolSize) {
                    /// create or initial poolsize
                    for (int i = 0; i < initialPoolSize; i++) {
                        connections.push_back(createConn());
                    }
                }
            }
            catch (const ConfigException &ex) {
                throw ConfigException(ex.what());
            }
            catch (const std::runtime_error &e) {
                std::cout << "init pool crash \n";
                std::cout << typeid(T).name() << "\n";
                throw WinterException(e.what());
            }
            catch (...) {
                std::string what = "init pool crash";
                throw WinterException(what.data());
            }
        };
        std::thread db_pool_thread;
        db_pool_thread = std::thread(db_pool);
        db_pool_thread.detach();

    }


    template<typename T>
    T &ConnectionPool<T>::getConnection() {

        checkConnections();
        auto connElement = connections.front();
        moveConnection(connections, usedConnections, connElement);
        return *connElement;
    }

    template<typename T>
    void ConnectionPool<T>::checkConnections() {
        /// if we dont have connections availables we create new
        int intents = 0;
        if (connections.empty()) {
            ///only if is less that our maxPoolSize if not.. throw exception
            if (usedConnections.size() < maxPoolSize) {
                try {
                    addConnections(createConn());
                } catch (std::runtime_error &e) {
                    throw WinterException(e.what());
                }
            } else {
                std::chrono::milliseconds waitTme(1000);
                while (connections.empty()) {
                    if (intents >= 60) {
                        throw WinterException("Maximum connection pool size and time reached, no available connections!");
                    }
                    std::cout << "wait for connection available\n";
                    std::this_thread::sleep_for(waitTme);
                    intents++;
                }
                std::cout << "connection found\n";
            }

        }
    }

    template<typename T>
    void ConnectionPool<T>::addConnections(T *conn) {
        connections.push_back(conn);
    }

    template<typename T>
    void ConnectionPool<T>::releaseConnection(T *conn) {
        /// if we finish the use of the connection we move it to available connections
        if (!usedConnections.empty()) {
            moveConnection(usedConnections, connections, conn);
        }
        std::cout << "connections pool information: \n";
        std::cout << typeid(T).name() << "\n";
        std::cout << "used: " << usedConnections.size() << "\n";
        std::cout << "free: " << connections.size() << "\n";
        std::cout << "*********************************\n";
    }

    template<typename T>
    void ConnectionPool<T>::moveConnection(vector<T *> &from, vector<T *> &to, T *conn) {
        to.push_back(conn);
        from.erase(std::find(from.begin(), from.end(), conn));
        // i need to check if this is best approach to remove the element
        //from.erase(std::remove(from.begin(), from.end(), conn), from.end());
    }

    template<typename T>
    void ConnectionPool<T>::close() {
        for (auto conn : connections) {
            delete conn;
        }

        for (auto &conn : usedConnections) {
            delete conn;
        }
    }

    template<typename T>
    ConnectionPool<T>::~ConnectionPool() {
        close();
    }

}

#endif //WINTER_CONNECTION_POOL_H
