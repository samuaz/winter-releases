
//
// Created by AZCONA VARGAS, SAMUEL EDUARDO [AG-Contractor/5000] on 2019-10-09.
//

#ifndef WINTERC_SQL_QUERY_H
#define WINTERC_SQL_QUERY_H
#include "../field_descriptor.h"
#include <string>

namespace winter {
    using namespace descriptor;

    class SqlQuery {
    private:
        std::string _query;
        std::vector<AbstractField *> _fields;
    public:

        SqlQuery() = default;

        explicit SqlQuery(std::string query);

        SqlQuery(std::string query, std::vector<AbstractField *> fields);

        void addValue(AbstractField *field);

        std::vector<AbstractField *> getFields();

        const std::string &getQuery() const;

        void setQuery(const std::string &sqlQuery);

        ~SqlQuery();
    };

}


#endif //WINTERC_SQL_QUERY_H
