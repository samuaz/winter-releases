//
// Created by AZCONA VARGAS, SAMUEL EDUARDO [AG-Contractor/5000] on 2019-09-27.
//

#ifndef WINTER_SQL_CONNECTION_TEMPLATE_H
#define WINTER_SQL_CONNECTION_TEMPLATE_H

#include <memory>
#include <mutex>
#include <functional>
#include "../field_descriptor.h"
#include "sql_query.h"

namespace winter {

    template<typename Children, typename ConnectionType, typename ResultSetType, typename Status>
    class SQLConnection {

    protected:
        std::unique_ptr<ConnectionType> _conn;
        std::mutex _conn_mtx;
        std::function<void(Children *conn)> _releaseFunction;

        virtual void reconnect() = 0;

    public:
        SQLConnection() = default;

        explicit SQLConnection(ConnectionType *conn, std::function<void(Children *conn)> fun) : _conn(conn),
                                                                                                _releaseFunction(
                                                                                                        std::move(
                                                                                                                fun)) {}
        void setConn(ConnectionType *connection) {
            _conn = std::unique_ptr<ConnectionType>(connection);
        }

        void setReleaseFunction(std::function<void(Children *conn)> fun) {
            _releaseFunction = std::move(fun);
        }

        virtual std::shared_ptr<ResultSetType> select(SqlQuery &query) = 0;

        virtual Status update(SqlQuery &query) = 0;

        virtual Status insert(SqlQuery &query) = 0;

        void release(Children *conn) {
            _releaseFunction(conn);
            _conn_mtx.unlock();
        }

        virtual ~SQLConnection() {
            _conn.reset(nullptr);
        }

    };
}

#endif //WINTER_SQL_CONNECTION_TEMPLATE_H
