//
// Created by samuel on 04/09/19.
//

#ifndef WINTER_SQL_REPOSITORY_TEMPLATE_H
#define WINTER_SQL_REPOSITORY_TEMPLATE_H

#include <string>
#include "../repository_template.h"
#include "../../service/repository_status.h"
#include "../field_descriptor.h"
#include "sql_query.h"

namespace winter {

    template<typename EntityClass, typename IDType, typename ConnectionType, typename ResultSetType>
    class SqlRepository : public virtual Repository<EntityClass, IDType, SqlQuery, ResultSetType> {
    protected:

        virtual ConnectionType *getConn() = 0;

        virtual const RepositoryStatus insert(SqlQuery &query) = 0;

        virtual const RepositoryStatus update(SqlQuery &query) = 0;

        virtual std::shared_ptr<ResultSetType> select(SqlQuery &query) = 0;

        virtual EntityClass selectOne(SqlQuery &query) = 0;

        virtual std::vector<EntityClass> selectAll(SqlQuery &query) = 0;

        virtual EntityClass generate(std::shared_ptr<ResultSetType> res) = 0;

        virtual EntityClass createEntity(std::shared_ptr<ResultSetType> res) = 0;

        virtual ~SqlRepository() = default;

    };

}

#endif //WINTER_REPOSITORY_TEMPLATE_H
