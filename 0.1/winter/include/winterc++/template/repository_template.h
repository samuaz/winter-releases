//
// Created by samuel on 04/09/19.
//

#ifndef WINTER_REPOSITORY_TEMPLATE_H
#define WINTER_REPOSITORY_TEMPLATE_H

#include <string>

namespace winter {

    template<typename EntityClass, typename IDType, typename QueryType, typename ResultSetType>
    class Repository {

    public:
        virtual EntityClass findById(const IDType &id) = 0;

    protected:

        virtual EntityClass save(const EntityClass &entity) = 0;

        virtual EntityClass generate(std::shared_ptr<ResultSetType> res) = 0;

        virtual EntityClass createEntity(std::shared_ptr<ResultSetType> res) = 0;

        virtual EntityClass selectOne(QueryType &query) = 0;

        virtual std::vector<EntityClass> selectAll(QueryType &query) = 0;

        virtual ~Repository() = default;

    };
}

#endif //WINTER_REPOSITORY_TEMPLATE_H
