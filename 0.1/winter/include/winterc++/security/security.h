/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef WINTER_SECURITY_H
#define WINTER_SECURITY_H
#define CREATE_TOKEN(_USER_ID_) Security::Token::createToken(_USER_ID_)
#define CREATE_REFRESH_TOKEN(_USER_ID_) Security::Token::createToken(_USER_ID_)


#include <chrono>
#include <grpcpp/impl/codegen/string_ref.h>
#include <grpcpp/server_context.h>
#include "../include/jwt/jwt.hpp"
#include "token_status.h"
#include "../model/security/user_security_info.h"

namespace winter {

/**
 * this is the key value saved in redis that contains the user id
 */
#define USER_ID_KEY "userId"

/**
 * expiration key
 */
#define EXP_KEY "exp"

/**
 * algorithm to encode and decode
 */
#define ALGORITHM "HS256"

#define SECURE Security::secure(*context)

    using grpc::ServerContext;

/**
 * @namespace Security
 */
    namespace Security {

        /**
         * this method permit make any funciton securized only call Security::secure() in your function an this will check if user is auth if not
         * the request is rejected and exception throw, the exception is captured via response entity an send unauthenticated status to the client
         * @param context
         * @return
         */
        const UserSecurityInfo secure(const ServerContext &context);

        /**
         * @namespace Token
         * all utilities related to the token
         */
        namespace Token {

            /**
             * create user token that permit authentication you need To send the user id as parameter
             * @param userId
             * @return
             */
            const std::string createToken(const std::string &userId);


            /**
             * create refresh token that permit faster re-authentication and generates new token
             * @param userId
             * @return
             */
            const std::string createRefreshToken(const std::string &userId);

            /**
             * decode token and generate jwt object that contains information encoded in the token
             * @param token
             * @return
             */
            const jwt::jwt_object decodeToken(const std::string &token);

            /**
             * return the userId from token
             * @param token
             * @return
             */
            const std::string getUserFromToken(const std::string &token);

            /**
             * check if our token is valid and not expired
             * @param token
             * @return
             */
            const TokenStatus validateToken(const std::string &token);

            /**
             * extract the authorization Bearer from the grpc call metadata
             * @param metadata
             * @return
             */
            const std::string
            extractTokenFromGrpcMetadata(const std::multimap<grpc::string_ref, grpc::string_ref> &metadata);

            bool invalidChar(char c);

            void stripUnicode(std::string &str);
        }

        /**
         * @namespace Password
         * encode the user password before save in the database
         */
        namespace Password {
            const std::string encode(const std::string &password);

        }


    }
}
#endif //WINTER_SECURITY_H
