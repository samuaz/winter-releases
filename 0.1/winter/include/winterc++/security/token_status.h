/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */

#ifndef WINTER_TOKEN_STATUS_H
#define WINTER_TOKEN_STATUS_H


#include <string>
namespace winter {

/**
 * @enum VALID
 * @enum EXPIRED
 * @enum INVALID
 * status response from token
 */
    enum class TokenStatusType {
        VALID = 0,
        EXPIRED = 1,
        INVALID = 2
    };


/**
 * @struct TokenStatus
 */
    struct TokenStatus {
        bool valid;
        std::string message;
        TokenStatusType status;

    };
}

#endif //WINTER_TOKEN_STATUS_H
