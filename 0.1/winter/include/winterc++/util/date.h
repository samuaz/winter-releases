/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef USER_DATE_H
#define USER_DATE_H


#include <string>
#include <cstdio>
#include <iomanip>

/**
 * @class Date
 * simulate java Date functionality WORK IN PROGRESS
 */
namespace winter {

    class Date {

    private:

        int year = 0;
        int month = 0;
        int day = 0;
        int hour = 0;
        int minute = 0;
        int second = 0;

    public:

        Date();

        explicit Date(const std::string &date);

        Date(int year, int month, int day);

        Date(int year, int month, int day, int hour, int minute, int second);

        int getYear() const;

        void setYear(int year);

        int getMonth() const;

        void setMonth(int month);

        int getDay() const;

        void setDay(int day);

        int getHour() const;

        void setHour(int hour);

        int getMinute() const;

        void setMinute(int minute);

        int getSecond() const;

        void setSecond(int second);

        bool operator==(const Date &ref) const;

        bool operator!=(const Date &ref) const;

        bool operator<(const Date &rhs) const;

        bool operator>(const Date &rhs) const;

        bool operator<=(const Date &rhs) const;

        bool operator>=(const Date &rhs) const;


        std::string toString() const;


    };

}
#endif //USER_DATE_H
