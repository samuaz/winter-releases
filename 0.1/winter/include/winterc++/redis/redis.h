/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef WINTER_REDIS_H
#define WINTER_REDIS_H

#include <cpp_redis/cpp_redis>
#include "../template/connection_pool_template.h"
#include "redis_connection.h"

/**
 * @class Redis
 * Redis singleton class to save sessions and other needed cache things
 * i need to check if i need to make this multi thread and thread safe
 */
namespace winter::redis_impl {

        class Redis final : public virtual ConnectionPool<redis_impl::Connection> {


        public:

            static Redis &
            getInstance() {
                std::call_once(m_once, []() {
                    instance.reset(new Redis());
                });
                return *instance;
            }

            /**
             * init our connection pool creation in background detach thread, it use the initialpool and max pool sizes
             */
            static void init();

            redis_impl::Connection &connection();

            static void releaseConn(redis_impl::Connection *conn);

            redis_impl::Connection *createConn() override;

            /**
            * avoid operators to copy or singleton if you try to copy delete is called
            */
            Redis(Redis const &) = delete;

            void operator=(Redis const &) = delete;

            ~Redis() override;

        private:
            Redis();

            static std::unique_ptr<Redis> instance;

            /**
             * the be sure that our singleton is called only one time
             */
            static std::once_flag m_once;




        };
    }

#endif //WINTER_REDIS_H
