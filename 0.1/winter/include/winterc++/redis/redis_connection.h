//
// Created by samuel on 06/08/19.
//

#ifndef WINTER_REDIS_CONNECTION_H
#define WINTER_REDIS_CONNECTION_H
#include <cpp_redis/core/client.hpp>

namespace winter::redis_impl {

    //REDIS
#define REDIS_CONN(_FUNCTION_)std::invoke([&](){ \
redis_impl::Connection &redis = winter::redis_impl::Redis::getInstance().connection(); \
redis._FUNCTION_; \
})

#define REDIS_SET(_KEY_, _VALUE_) REDIS_CONN(set(_KEY_, _VALUE_))

#define REDIS_DEL(_KEY_) REDIS_CONN(delKey(_KEY_))

#define REDIS_GET_STRING(_KEY_) REDIS_CONN(getString(_KEY_))

        class Connection {
        private:
            cpp_redis::client *_conn{};
            std::mutex conn_mtx;
            std::string _host;
            size_t _port{};
            std::string _password;

            void reconnect();

        public:

            Connection() = default;

            explicit Connection(cpp_redis::client *connection);

            void setConn(cpp_redis::client *connection);

            void setHost(const std::string &host);

            void setPort(const size_t &port);

            void setPassword(const std::string &password);

            /**
             * set key and value to save in redis
             */
            void set(const std::string &key, const std::string &value);

            /**
             * get int value from key sync
             * @param key
             * @return
             */
            int getInt(const std::string &key);

            /**
             *
             * get string value from key sync
             * @param key
             * @return
             */
            std::string getString(const std::string &key);


            /**
            *
            * get string value from key with callback async
            * @param key
            * @return
            */
            void getStringCallback(const std::string &key, std::function<void(cpp_redis::reply &)> &reply_callback);

            /**
             * remove key from redis
             * @param key
             */
            void delKey(const std::string &key);

            void delKey(const std::vector<std::string> &key);

            virtual ~Connection();

        };
    }
#endif //WINTER_REDIS_CONNECTION_H
