/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef USER_STATUSSERVICE_H
#define USER_STATUSSERVICE_H

#include <cstdlib>
#include <iostream>

namespace winter {

/**
 * @enum SUCCESS 0
 * @enum FAILED 1
 * @enum SQLERROR 2
 */
    enum class RepositoryStatusType {
        SUCCESS = 0,
        FAILED = 1,
        DBERROR = 2
    };

    struct RepositoryStatus {

        RepositoryStatusType status;
        std::string message;

    };
}
#endif //USER_STATUSSERVICE_H
