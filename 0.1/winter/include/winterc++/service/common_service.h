/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef USER_COMMONSERVICE_H
#define USER_COMMONSERVICE_H
#include <mysql/jdbc.h>
#include <grpcpp/support/status.h>
#include "repository_status.h"

namespace winter::CommonService {
    //RESPONSE MACRO FOR GRPC
#define RESPONSE_ENTITY(...) std::function<void()> service_call = [&](){__VA_ARGS__}; \
return CommonService::ResponseEntity(service_call)

        /**
         * auto generate response for the grpc client also this is exception safe
         * @param execute
         * @return
         */
        const grpc::Status ResponseEntity(const std::function<void()> &execute);

    }

#endif //USER_COMMONSERVICE_H