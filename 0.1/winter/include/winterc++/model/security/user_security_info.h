//
// Created by samuel on 08/08/19.
//

#ifndef WINTER_USER_SECURITY_INFO_H
#define WINTER_USER_SECURITY_INFO_H

#include <string>

namespace winter {

    struct UserSecurityInfo {
        std::string userId;
        std::string token;
        bool valid;
    };

}


#endif //WINTER_USER_SECURITY_INFO_H
