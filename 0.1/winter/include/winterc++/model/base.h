/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */

#ifndef USER_BASE_H
#define USER_BASE_H

#include <string>
#include <ctime>
#include "../util/date.h"


/**
 * @class Base
 * base entity class
 */

namespace winter {

    class Base {

    public:
        Base();

        explicit Base(std::string id);

    protected:
        std::string id;
        Date creationDate;
        Date modificationDate;

    public:

        const std::string &getId() const;

        void setId(const std::string &id);

        const Date &getCreationDate() const;

        void setCreationDate(const Date &creationDate);

        const Date &getModificationDate() const;

        void setModificationDate(const Date &modificationDate);

        bool operator==(const Base &rhs) const;

        bool operator!=(const Base &rhs) const;

        bool operator<(const Base &rhs) const;

        bool operator>(const Base &rhs) const;

        bool operator<=(const Base &rhs) const;

        bool operator>=(const Base &rhs) const;

        virtual bool equals(Base *ref) const = 0;

        virtual ~Base();


    };
}


#endif //USER_BASE_H
