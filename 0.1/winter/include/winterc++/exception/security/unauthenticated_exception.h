/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef WINTER_UNAUTHENTICATED_EXCEPTION_H
#define WINTER_UNAUTHENTICATED_EXCEPTION_H


#include "../generic/winter_exception.h"
namespace winter {

    class UnauthenticatedException final : public WinterException {
    public:
        explicit UnauthenticatedException(char const *message) noexcept;
    };
}


#endif //WINTER_UNAUTHENTICATED_EXCEPTION_H
