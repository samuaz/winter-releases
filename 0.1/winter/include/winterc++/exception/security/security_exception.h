/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef WINTER_SECURITY_EXCEPTION_H
#define WINTER_SECURITY_EXCEPTION_H

#include "../generic/winter_exception.h"

namespace winter {

    class SecurityException final : public WinterException {
    public:
        explicit SecurityException(char const *message) noexcept;
    };

}

#endif //WINTER_SECURITY_EXCEPTION_H
