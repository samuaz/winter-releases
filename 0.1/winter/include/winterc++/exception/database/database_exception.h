/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef WINTER_DATABASE_EXCEPTION_H
#define WINTER_DATABASE_EXCEPTION_H


#include "../generic/winter_exception.h"

namespace winter {

    class DatabaseException final : public WinterException {
    public:
        explicit DatabaseException(char const *message) noexcept;
    };

}

#endif //WINTER_DATABASE_EXCEPTION_H
