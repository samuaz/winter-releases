/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef USER_CONFIGEXCEPTION_H
#define USER_CONFIGEXCEPTION_H

#include "../generic/winter_exception.h"

namespace winter {

    class ConfigException final : public WinterException {
    public:
        explicit ConfigException(char const *message) noexcept;
    };
}


#endif //USER_CONFIGEXCEPTION_H
