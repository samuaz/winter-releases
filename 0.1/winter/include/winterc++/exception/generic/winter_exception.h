/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef WINTER_EXCEPTION_H
#define WINTER_EXCEPTION_H

#include <stdexcept>

using namespace std;
namespace winter {

    class WinterException : public std::runtime_error {
    public:
        explicit WinterException(char const *message) noexcept;
    };
}

#endif //WINTER_EXCEPTION_H
