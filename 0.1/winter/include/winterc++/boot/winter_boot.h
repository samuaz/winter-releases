//
// Created by AZCONA VARGAS, SAMUEL EDUARDO [AG-Contractor/5000] on 2019-10-08.
//

#ifndef WINTER_WINTER_BOOT_H
#define WINTER_WINTER_BOOT_H

#include <grpcpp/grpcpp.h>
#include "../config/config.h"
#include "../mysql/mysql_pool.h"
#include "../redis/redis.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

#define WINTER_SET_ENV(_ENV_) winter::WinterBoot::setEnv(_ENV_)
#define WINTER_READ_CONFIG(_CONFIG_) winter::WinterBoot::setConfigString(_CONFIG_)
#define WINTER_ADD_CONTROLLER(_SERVICE_) _SERVICE_ controller##_SERVICE_; \
winter::WinterBoot::registerController(&controller##_SERVICE_)
#define WINTER_INIT winter::WinterBoot::init()

namespace winter {

    class WinterBoot {

    public:

        static WinterBoot &
        getInstance() {
            std::call_once(m_once, []() {
                instance.reset(new WinterBoot());
            });
            return *instance;
        }

        static void init();

        WinterBoot(WinterBoot const &) = delete;

        void operator=(WinterBoot const &) = delete;

        ~WinterBoot();

        static void registerController(grpc::Service * controller);

        static void setConfigDir(const std::string &configAbsolutePath);

        static void setConfigString(const std::string &yamlString);

        static void setEnv(const std::string &env);


    private:
        WinterBoot();
        static ServerBuilder builder;
        static std::unique_ptr<WinterBoot> instance;
        static std::once_flag m_once;
        static void runServer();

    };
}


#endif //WINTER_WINTER_BOOT_H
