/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef USER_CONFIG_H
#define USER_CONFIG_H

/**
 * if you are using docker build the config file need be
 * in the system because is not part of the single binary
 * and you can mod it when you want
 *
 */

#include <string>
#include <yaml-cpp/yaml.h>

#define WINTER_CONFIG(VALUE) namespace winter {\
static const char * CONFIG =  VALUE;\
}

namespace winter {

    class Config {

    private:
        Config() = default;
        static Config *config;
        YAML::Node file;
        static std::string ENV;
    public:
        Config(Config const &) = delete;

        void operator=(Config const &) = delete;

        virtual ~Config();

        static void loadConfigFromFile(const std::string &absolutePath);

        static void loadConfigFromString(const std::string &yamlString);

        const static YAML::Node getSecurityConfig();

        const static YAML::Node getConfig();

        const static YAML::Node getDbConfig();

        const static YAML::Node getRedisConfig();

        const static YAML::Node getGrpcConfig();

        static void setEnvironment(const std::string &environment);


    };
}

#endif //USER_CONFIG_H
