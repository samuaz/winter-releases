/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */

#ifndef WINTER_MYSQL_CONNECTION_H
#define WINTER_MYSQL_CONNECTION_H

#include "../template/sql/sql_connection_template.h"
#include "../template/field_descriptor.h"
#include "../service/repository_status.h"
#include <mysql/jdbc.h>

namespace winter::mysql_impl {

        class Connection
                : public SQLConnection<mysql_impl::Connection, sql::Connection, sql::ResultSet, RepositoryStatus> {

        public:
            std::shared_ptr<sql::ResultSet> select(SqlQuery &query) override;

            RepositoryStatus update(SqlQuery &query) override;

            RepositoryStatus insert(SqlQuery &query) override;

            sql::PreparedStatement *generatePrepareStatement(SqlQuery &query);

        protected:
            void reconnect() override;


        };

    }


#endif //WINTER_MYSQL_CONNECTION_H
