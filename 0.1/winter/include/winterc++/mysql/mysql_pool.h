/**
 * @author AZCONA VARGAS, SAMUEL EDUARDO - samuel@theopencompany.tech/samuaz@gmail.com
 * @YEAR 2019
 */


#ifndef WINTER_MYSQL_CONNECTION_POOL_NEW_H
#define WINTER_MYSQL_CONNECTION_POOL_NEW_H

#include <vector>
#include <thread>
#include <memory>
#include <mutex>
#include "mysql_connection.h"
#include "../template/connection_pool_template.h"


namespace winter::mysql_impl {

        /**
         * @class Database
         */
        class MysqlPool final : public virtual ConnectionPool<mysql_impl::Connection> {
        public:

            /**
             *  return and get our unique instance of this class
             */
            static MysqlPool &
            getInstance() {
                std::call_once(m_once, []() {
                    instance.reset(new MysqlPool());
                });
                return *instance;
            }

            /**
             * init our connection pool creation in background detach thread, it use the initialpool and max pool sizes
             */
            static void init();

            mysql_impl::Connection *createConn() override;


            /**
             * execute sql query and return connection with prepared statement
             * @param query
             * @return
             */
            mysql_impl::Connection &getConnection() override;

            /**
            * avoid operators to copy or singleton if you try to copy delete is called
            */
            MysqlPool(MysqlPool const &) = delete;

            void operator=(MysqlPool const &) = delete;

            ~MysqlPool() override;

        private:

            MysqlPool();

            static std::unique_ptr<MysqlPool> instance;

            static std::once_flag m_once;

            sql::ConnectOptionsMap connectionProperties;

        };
    }

#endif //USER_DATABASE_H
