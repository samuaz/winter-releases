//
// Created by AZCONA VARGAS, SAMUEL EDUARDO [AG-Contractor/5000] on 2019-09-27.
//

#ifndef WINTER_MYSQL_REPOSITORY_IMPL_H
#define WINTER_MYSQL_REPOSITORY_IMPL_H
#include <jdbc/cppconn/connection.h>
#include "../template/sql/sql_repository_template.h"
#include "mysql_connection.h"
#include "../exception/database/database_exception.h"
#include "mysql_pool.h"
#include "../redis/redis.h"

namespace winter {

    using namespace descriptor;

// VIRTUAL INPUT OF SERVICES THAT DONT HAVE IMPLEMENTATION
#define NOT_IMPLEMENTED return {}
// DB SETTERS
#define QUERY(_VALUE_) SqlQuery query(_VALUE_)
#define SET_STRING(_VALUE_) query.addValue(new Field<std::string>(_VALUE_, FieldType::STRING))
#define SET_INT(_VALUE_) query.addValue(new Field<int>(_VALUE_, FieldType::INT))
#define SET_BOOL(_VALUE_) query.addValue(new Field<bool>(_VALUE_, FieldType::BOOLEAN))
#define SET_DATE(_VALUE_) query.addValue(new Field<sql::SQLString>(_VALUE_, FieldType::DATE))
#define SET_DOUBLE(_VALUE_) query.addValue(new Field<double>(_VALUE_, FieldType::DOUBLE))
#define SET_ENUM(_VALUE_) query.addValue(new Field<int>(static_cast<int>(_VALUE_), FieldType::INT))

//DATABASE GETTERS
#define GET_STRING(_VALUE_) res->getString(_VALUE_)
#define GET_DATE(_VALUE_) res->getString(_VALUE_)
#define GET_INT(_VALUE_) res->getInt(_VALUE_)
#define GET_ENUM(_VALUE_) res->getInt(_VALUE_)
#define GET_BOOL(_VALUE_) res->getBoolean(_VALUE_)
#define GET_DOUBLE(_VALUE_) res->getDouble(_VALUE_)

// MYSQL INSERT QUERY
#define INSERT(_QUERY_, _EXCEPTION_) RepositoryStatus status = insert(_QUERY_); \
if (status.status > RepositoryStatusType::SUCCESS) { \
throw _EXCEPTION_(status.message.data()); \
}

//VECTOR TO REPEATEDFIELD
#define VECTOR_TO_PROTO(_REPEATED_FIELD_, _FUNC_THAT_GENERATE_VECTOR_) std::invoke([&](){ \
try { \
auto vec = _FUNC_THAT_GENERATE_VECTOR_; \
*_REPEATED_FIELD_ = {vec.begin(), vec.end()}; \
} catch (WinterException &ignore) { \
} \
})


    template<typename EntityClass, typename IDType, char const *EntityName, typename ExceptionType>
    class MysqlRepository : public virtual SqlRepository<EntityClass, IDType, mysql_impl::Connection, sql::ResultSet> {

    public:
        EntityClass findById(const IDType &id) override;

        virtual EntityClass save(const EntityClass &entity) override = 0;

    protected:

        mysql_impl::Connection *getConn() override;

        const RepositoryStatus insert(SqlQuery &query) override;

        const RepositoryStatus update(SqlQuery &query) override;

        std::shared_ptr<sql::ResultSet> select(SqlQuery &query) override;

        EntityClass selectOne(SqlQuery &query) override;

        vector<EntityClass> selectAll(SqlQuery &query) override;

        EntityClass generate(std::shared_ptr<sql::ResultSet> res) override;

        EntityClass createEntity(std::shared_ptr<sql::ResultSet> res) override = 0;

    };

    template<typename EntityClass, typename IDType, const char *EntityName, typename ExceptionType>
    mysql_impl::Connection *MysqlRepository<EntityClass, IDType, EntityName, ExceptionType>::getConn() {
        return &mysql_impl::MysqlPool::getInstance().getConnection();
    }

    template<typename EntityClass, typename IDType, const char *EntityName, typename ExceptionType>
    const RepositoryStatus MysqlRepository<EntityClass, IDType, EntityName, ExceptionType>::insert(SqlQuery &query) {
        auto conn = getConn();
        return conn->insert(query);
    }

    template<typename EntityClass, typename IDType, const char *EntityName, typename ExceptionType>
    const RepositoryStatus MysqlRepository<EntityClass, IDType, EntityName, ExceptionType>::update(SqlQuery &query) {
        auto conn = getConn();
        return conn->update(query);
    }

    template<typename EntityClass, typename IDType, const char *EntityName, typename ExceptionType>
    std::shared_ptr<sql::ResultSet>
    MysqlRepository<EntityClass, IDType, EntityName, ExceptionType>::select(SqlQuery &query) {
        auto conn = getConn();
        auto res = conn->select(query);
        return res;
    }

    template<typename EntityClass, typename IDType, char const *EntityName, typename ExceptionType>
    EntityClass
    MysqlRepository<EntityClass, IDType, EntityName, ExceptionType>::generate(std::shared_ptr<sql::ResultSet> res) {
        try {
            if (res != nullptr) {
                EntityClass value = createEntity(res);
                return value;
            } else {
                std::string message = std::string(EntityName) + " Not found";
                throw ExceptionType(message.data());
            }
        } catch (...) {
            std::string message = std::string(EntityName) + " Not found";
            throw ExceptionType(message.data());
        }

    }

    template<typename EntityClass, typename IDType, const char *EntityName, typename ExceptionType>
    EntityClass MysqlRepository<EntityClass, IDType, EntityName, ExceptionType>::selectOne(SqlQuery &query) {
        auto res = select(query);
        res->first();
        EntityClass entity = generate(res);
        return entity;

    }

    template<typename EntityClass, typename IDType, const char *EntityName, typename ExceptionType>
    vector<EntityClass> MysqlRepository<EntityClass, IDType, EntityName, ExceptionType>::selectAll(SqlQuery &query) {
        auto res = select(query);
        std::vector<EntityClass> entity;
        while (res->next()) {
            entity.push_back(generate(res));
        }
        return entity;
    }

    template<typename EntityClass, typename IDType, const char *EntityName, typename ExceptionType>
    EntityClass MysqlRepository<EntityClass, IDType, EntityName, ExceptionType>::findById(const IDType &id) {

        std::string type = TypeName<IDType>::Get();
        SqlQuery query;

        if (type == "string") {
            query.setQuery("Select * from " + std::string(EntityName) + " where id =unhex(?)");
            query.addValue(new Field<std::string>(id, FieldType::STRING));
        } else if (type == "int") {
            query.setQuery("Select * from " + std::string(EntityName) + " where id = ? ");
            query.addValue(new Field<IDType>(id, FieldType::INT));
        } else if (type == "long") {
            query.setQuery("Select * from " + std::string(EntityName) + " where id = ? ");
            query.addValue(new Field<IDType>(id, FieldType::LONG));
        } else {
            throw DatabaseException("unrecognized id");
        }

        return selectOne(query);

    }
}


#endif //WINTER_MYSQL_REPOSITORY_IMPL_H
