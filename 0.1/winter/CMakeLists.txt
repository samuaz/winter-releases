#
# Created by AZCONA VARGAS, SAMUEL EDUARDO
#

cmake_minimum_required(VERSION 3.10)
option(APP_NAME "APP_NAME" helloworld)
option(APP_ENV "APP_ENV" local)

option(HOST_OS "HOST_OS")

###############################
## CONFIG BUILD SYSTEM FILES ##
###############################
if (UNIX AND NOT APPLE)
    set(HOST_OS "linux")
endif ()

if (APPLE)
    set(HOST_OS "apple")
endif ()

set(WINTER_LIB_DIR "${PROJECT_SOURCE_DIR}/winter/lib/${HOST_OS}")
set(WINTER_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/winter/include")
set(WINTER_THIRD_PARTY "${PROJECT_SOURCE_DIR}/winter/include/winterc++/third_party")
set(WINTER_PROTO_BINARY "${PROJECT_SOURCE_DIR}/winter/include/winterc++/third_party/protobuilder/${HOST_OS}")

if (APP_NAME)
    message("***************************** BUILDING ${APP_NAME} **************************************")
else ()
    message("***************************** NOT APP_NAME **************************************")
    message("you need to tellme what is the name of your project/app")
    message("example:")
    message("-DAPP_NAME=helloworld")
    message(FATAL_ERROR "PLESE ADD A APPNAME")
endif (APP_NAME)


message("***************************** ENVIRONMENT IS ${APP_ENV} **************************************")
if (APP_ENV)
else ()
    message("***************************** NOT ENV **************************************")
    message("local AS DEFAULT")
    set(APP_ENV local)
endif (APP_ENV)


#configure_file(${PROJECT_SOURCE_DIR}/winter/config/env.h.in ${CMAKE_BINARY_DIR}/generated/env.h)
#include_directories(${CMAKE_BINARY_DIR}/generated)

############################
##      SOURCE FILES      ##
############################
include_directories(${PROJECT_SOURCE_DIR}/winter/include)
include_directories(${PROJECT_SOURCE_DIR}/winter/include/winterc++/third_party/include)
include_directories(/usr/local/include)
############################
##    END SOURCE FILES    ##
############################

project(${APP_NAME}-${APP_ENV} LANGUAGES CXX)
message("***************************** APP NAME IS **************************************")
message(${PROJECT_NAME})


############################
##    PROTOBUF FILES      ##
############################
# this do:
# - search al .proto files in the PROTOBUF_INPUT_DIRECTORY
# - we do a foreach in the list of all proto files
# - because or services use grpc so we need to build services with grpc plugin so we search for files that contains service word to create the grpc files
# - we build all our proto models and services
# - we add all our generated clases and headers to the include sources
message("***************************** CREATING PROTOBUF **************************************")

## protobuf generator permissions

execute_process(COMMAND chmod +x ${WINTER_PROTO_BINARY}/protoc
        WORKING_DIRECTORY ..
        RESULT_VARIABLE PROTOBUF_PERMISSION_RESULT
        OUTPUT_VARIABLE PROTOBUF_PERMISSION_OUTPUT_VARIABLE)
MESSAGE(STATUS "CMD_ERROR:" ${PROTOBUF_PERMISSION_RESULT})
MESSAGE(STATUS "CMD_OUTPUT:" ${PROTOBUF_PERMISSION_OUTPUT_VARIABLE})

execute_process(COMMAND chmod +x ${WINTER_PROTO_BINARY}/grpc_cpp_plugin
        WORKING_DIRECTORY ..
        RESULT_VARIABLE PROTOBUF_PERMISSION_RESULT
        OUTPUT_VARIABLE PROTOBUF_PERMISSION_OUTPUT_VARIABLE)
MESSAGE(STATUS "CMD_ERROR:" ${PROTOBUF_PERMISSION_RESULT})
MESSAGE(STATUS "CMD_OUTPUT:" ${PROTOBUF_PERMISSION_OUTPUT_VARIABLE})

file(GLOB PROTOBUF_DEFINITION_FILES "${PROJECT_SOURCE_DIR}/src/proto/*.proto")
set(PROTOBUF_INPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/src/proto")
set(PROTOBUF_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/src/model/proto")
foreach (file ${PROTOBUF_DEFINITION_FILES})
    message("***************************** CREATING ${file} IN ${PROTOBUF_OUTPUT_DIRECTORY}  **************************************")
    if (file MATCHES "service")
        MESSAGE(${file} is service)
        execute_process(COMMAND ${WINTER_PROTO_BINARY}/protoc -I ${PROTOBUF_INPUT_DIRECTORY} --grpc_out=${PROTOBUF_OUTPUT_DIRECTORY} --plugin=protoc-gen-grpc=${WINTER_PROTO_BINARY}/grpc_cpp_plugin ${file}
                WORKING_DIRECTORY ..
                RESULT_VARIABLE PROTOBUF_RESULT
                OUTPUT_VARIABLE PROTOBUF_OUTPUT_VARIABLE)
        MESSAGE(STATUS "CMD_ERROR:" ${PROTOBUF_RESULT})
        MESSAGE(STATUS "CMD_OUTPUT:" ${PROTOBUF_OUTPUT_VARIABLE})
        execute_process(COMMAND ${WINTER_PROTO_BINARY}/protoc -I ${PROTOBUF_INPUT_DIRECTORY} --cpp_out=${PROTOBUF_OUTPUT_DIRECTORY} ${file}
                WORKING_DIRECTORY ..
                RESULT_VARIABLE PROTOBUF_RESULT
                OUTPUT_VARIABLE PROTOBUF_OUTPUT_VARIABLE)
        MESSAGE(STATUS "CMD_ERROR:" ${PROTOBUF_RESULT})
        MESSAGE(STATUS "CMD_OUTPUT:" ${PROTOBUF_OUTPUT_VARIABLE})
    else ()
        MESSAGE(${file} is model)
        execute_process(COMMAND ${WINTER_PROTO_BINARY}/protoc -I ${PROTOBUF_INPUT_DIRECTORY} --cpp_out=${PROTOBUF_OUTPUT_DIRECTORY} ${file}
                WORKING_DIRECTORY ..
                RESULT_VARIABLE PROTOBUF_RESULT
                OUTPUT_VARIABLE PROTOBUF_OUTPUT_VARIABLE)
        MESSAGE(STATUS "CMD_ERROR:" ${PROTOBUF_RESULT})
        MESSAGE(STATUS "CMD_OUTPUT:" ${PROTOBUF_OUTPUT_VARIABLE})
    endif (file MATCHES "service")
endforeach ()

file(GLOB PROTOBUF_MODELS_INCLUDES "${PROJECT_SOURCE_DIR}/src/model/proto/*.cc" "${PROJECT_SOURCE_DIR}/src/model/proto/*.h")

############################
##    END PROTOBUF FILES  ##
############################



############################
##      CONFIG BUILD      ##
############################

# with release we use optimizations also we strip the final binary because this is single static exec to run inside minimal alpine docker
# and google cloud run designed also can run on vm or whatever that support dockerfile

# if build Type not set build in release allways
if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif ()

# build mode its required to set de env version to deploy  local|Docker
# if we build in local mode we build shared library not static
option(BUILD_MODE "Use BUILD_MODE" Local) #LOCAL by default

if (BUILD_MODE)
else ()
    message("***************************** NOT BUILD MODE **************************************")
    message("you need to tellme what mode you want to build possible values: local|docker")
    message("example:")
    message("-DBUILD_MODE=Local|Docker")
    message("USING Local AS DEFAULT")
endif (BUILD_MODE)
add_definitions( -DBUILD_MODE=${BUILD_MODE} )

message("***************************** BUILDING FOR ${BUILD_MODE} **************************************")
if (BUILD_MODE STREQUAL "Docker")
    # in docker mode we build all libs in static and single binary
    message("***************************** USING STATIC LIBS **************************************")
    add_compile_definitions(STATIC_CONCPP)
    set(BUILD_SHARED_LIBS OFF)
    set(CMAKE_EXE_LINKER_FLAGS ${CMAKE_EXE_LINKER_FLAGS} "-static")
    set(DCMAKE_FIND_LIBRARY_SUFFIXES .a)
    set(winterc++ ${WINTER_LIB_DIR}/libwinterc++.a)
    set(MY_LIBS ${winterc++})
else ()
    message("***************************** USING SHARED LIBS **************************************")
    set(winterc++ ${WINTER_LIB_DIR}/libwinterc++.a)
    set(MY_LIBS ${winterc++})
endif (BUILD_MODE STREQUAL "Docker")
#configure_file(${PROJECT_SOURCE_DIR}/winter/config/config_dir.h.in ${CMAKE_BINARY_DIR}/generated/config_dir.h)
############################
##    END CONFIG BUILD    ##
############################

############################
##         FLAGS          ##
############################

if (UNIX AND NOT APPLE)
    message("UNIX FLAGS")
    set(EXTRA_LIBRARY "-pthread -lstdc++ -lrt -lssl -lz -ldl")
endif ()

if (APPLE)
    message("MAC FLAGS")
    set(EXTRA_LIBRARY "-pthread -lstdc++ -pthread")
endif ()

set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -s")
set(CMAKE_CXX_FLAGS "-pipe -fno-unroll-loops -fmerge-all-constants -fno-ident -ffunction-sections -fdata-sections -Bsymbolic-functions -s")
set(CMAKE_CXX_FLAGS_DEBUG "-O3 -g -pedantic-errors -Wall -Wextra ")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

############################
##       END FLAGS        ##
############################

############################
##    CREATE EXECUTABLE   ##
############################
message("***************************** LIBRARIE ${MY_LIBS} **************************************")
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}
        DESTINATION bin/${PROJECT_NAME}/ ${CMAKE_INSTALL_LIBDIR})
install(FILES winter/config/config.yaml DESTINATION bin/${PROJECT_NAME}/ COMPONENT config.yaml)

############################
##  END CREATE EXECUTALBE ##
############################
